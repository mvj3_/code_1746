//SlidingDraw已经被摒弃了，但是在某些地方还是可以用一用的

//1.主界面布局文件activity_main.xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="fill_parent"
    android:layout_height="fill_parent" >
    <SlidingDrawer
        android:id="@+id/sliding"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:content="@+id/allApps"
        android:handle="@+id/imageViewIcon"
        android:orientation="horizontal" >
        <GridView
            android:id="@+id/allApps"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:columnWidth="60dp"
            android:gravity="center"
            android:horizontalSpacing="10dp"
            android:numColumns="8"
            android:padding="10dp"
            android:stretchMode="columnWidth"
            android:verticalSpacing="15dp" />
        <include layout="@layout/nine_path"/>
    </SlidingDrawer> 
</RelativeLayout>
//2.显示所有已安装应用程序的图标和名称的布局文件nine_path.xml
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="fill_parent"
    android:layout_height="fill_parent" >

    <ImageView
        android:id="@+id/imageViewIcon"
        android:layout_width="@android:dimen/app_icon_size"
        android:layout_height="@android:dimen/app_icon_size"
        android:layout_centerHorizontal="true"
        android:scaleType="fitCenter"
        android:src="@drawable/arrow_left" />

    <TextView
        android:id="@+id/text"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:gravity="center_horizontal"
        android:paddingTop="5dip"
        android:layout_below="@+id/imageViewIcon"
        android:layout_centerHorizontal="true" />

</RelativeLayout>
//3.实现代码：
package com.example.slidingdraw;

import java.util.List;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.SlidingDrawer;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	private GridView gv;
	@SuppressWarnings("deprecation")
	private SlidingDrawer sd;
	private ImageView iv;
	private List<ResolveInfo> apps;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		loadApps();
		
		gv = (GridView) findViewById(R.id.allApps);
        sd = (SlidingDrawer) findViewById(R.id.sliding);
        iv = (ImageView) findViewById(R.id.imageViewIcon);
        gv.setAdapter(new GridAdapter(MainActivity.this));
        gv.setOnItemClickListener(listener);
        sd.setOnDrawerOpenListener(new SlidingDrawer.OnDrawerOpenListener()// open drawer
        {
            @Override
            public void onDrawerOpened() {
                iv.setImageResource(R.drawable.arrow_right);// listener of open
            }
        });
        sd.setOnDrawerCloseListener(new SlidingDrawer.OnDrawerCloseListener() {// close drawer
            @Override
            public void onDrawerClosed() {
                iv.setImageResource(R.drawable.arrow_left);// listener of close
            }
        });
	}

	private void loadApps() {
        Intent intent = new Intent(Intent.ACTION_MAIN, null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        apps = getPackageManager().queryIntentActivities(intent, 0);
    }
	
public class GridAdapter extends BaseAdapter {
		
		class InfoHolder {
			ImageView imageView;
			TextView textView;
		}
		
		Context mContext;
	    LayoutInflater mInflater;
		
        public GridAdapter(Context c) {
        	super();
            mContext = c;
            mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        
        public int getCount() {
            return apps.size();
        }
        
        public Object getItem(int position) {
            return apps.get(position);
        }
        
        public long getItemId(int position) {
            return position;
        }
        
        public View getView(int position, View convertView, ViewGroup parent) {
        	InfoHolder holder;
            if (convertView == null) {
            	convertView = mInflater.inflate(R.layout.nine_path, null);
            	holder = new InfoHolder();
                holder.imageView = (ImageView) convertView.findViewById(R.id.imageViewIcon);
                holder.textView = (TextView) convertView.findViewById(R.id.text);
                convertView.setTag(holder);
            } else {
            	holder = (InfoHolder) convertView.getTag();
            }
 
            String appName = apps.get(position).activityInfo.loadLabel(getPackageManager()).toString();
            Drawable appIcon = apps.get(position).activityInfo.loadIcon(getPackageManager());
            AppsInfo mAppsInfo = new AppsInfo(appName, appIcon);
            if (mAppsInfo != null) {
                holder.imageView.setImageDrawable(mAppsInfo.getAppIcon());
                holder.textView.setText(mAppsInfo.getAppLable());
            }
            
            return convertView;
        }
 
    }
	
	public class AppsInfo {
	    String appLable;
	    Drawable appIcon;

	    public AppsInfo() {

	    }

	    public AppsInfo(String appLable, Drawable appIcon) {
	        this.appLable = appLable;
	        this.appIcon = appIcon;
	    }

	    public void setAppLable(String appLable) {
	        this.appLable = appLable;
	    }

	    public String getAppLable() {
	        return this.appLable;
	    }

	    public void setAppIcon(Drawable appIcon) {
	        this.appIcon = appIcon;
	    }

	    public Drawable getAppIcon() {
	        return this.appIcon;
	    }
	}
	
	private OnItemClickListener listener = new OnItemClickListener() {

	    @Override
	    public void onItemClick(AdapterView<?> parent, View view, int position,
	            long id) {
	    	
	        ResolveInfo mResolveInfo = apps.get(position);

	        //package name
	        String packageName = mResolveInfo.activityInfo.packageName;

	        //class name
	        String className = mResolveInfo.activityInfo.name;

	        ComponentName mComponentName = new ComponentName(packageName, className);
	        Intent i = new Intent();
	        if (mComponentName != null) {
	            i.setComponent(mComponentName);
	        }
	        startActivity(i);

	    }
	};

}